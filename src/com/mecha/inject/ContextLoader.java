package com.mecha.inject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ContextLoader {

    static ClassesInDir classesInDir = new ClassesInDir("src");
    static DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
    Document xmlDocument;
    Map<Class, Object> instanceOfClasses = new HashMap<>();

    public ContextLoader (String xmlFilePath) {
        try {
            xmlDocument = documentFactory.newDocumentBuilder().parse(xmlFilePath);
            xmlDocument.getDocumentElement().normalize();
        } catch (SAXException|IOException|ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void displayBeanContents(Node aBean) {
        NodeList nodeList = aBean.getChildNodes();
        for (int i = 0; i < aBean.getChildNodes().getLength(); i++) {
            System.out.println(nodeList.item(i).getTextContent());
        }
    }

    private Object getBeanInstance(Element aBean) {
        Object anInstance = null;
        try {
            anInstance = classesInDir.getClass(aBean.getAttribute("className")).newInstance();
        } catch (ClassNotFoundException|IllegalAccessException|InstantiationException e) {
            e.printStackTrace();
        }

        Method[] methods = anInstance.getClass().getMethods();
        NodeList nodeList = aBean.getChildNodes();

        for (Method method : methods) {
            if (method.getName().contains("set")) {
                String fieldName = getFieldNameToSet(method);
                for (int i = 1; i < nodeList.getLength(); i++) {
                    if (nodeList.item(i).getNodeName().equals("property") && nodeList.item(i).getAttributes().item(0).getTextContent().equals(fieldName)) {
                        if (nodeList.item(i).getChildNodes().getLength() > 1) {
                            try {
                                method.invoke(anInstance,getBeanInstance((Element) nodeList.item(i).getChildNodes().item(1)));
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        } else {
                            setFieldAsItType(i,nodeList ,anInstance, method);
                        }
                    }
                }
            }
        }
        return anInstance;
    }

    private void setFieldAsItType(int i, NodeList nodeList, Object anInstance, Method method) {
        Type[] types = method.getParameterTypes();
        try {
            if (types[0].getTypeName().contains("String"))
                method.invoke(anInstance,(String)(nodeList.item(i).getTextContent()));
            else if (types[0].getTypeName().contains("Class"))
                method.invoke(anInstance,Class.forName(nodeList.item(i).getTextContent()));
            else if (types[0].getTypeName().contains("int"))
                method.invoke(anInstance,Integer.parseInt(nodeList.item(i).getTextContent()));
            else if (types[0].getTypeName().contains("double"))
                method.invoke(anInstance,Double.parseDouble(nodeList.item(i).getTextContent()));
        } catch (IllegalAccessException | InvocationTargetException | ClassNotFoundException  e) {
            e.printStackTrace();
        }
    }

    private void setInstanceOfClasses(Element root) {
        NodeList beans = root.getChildNodes();

        for (int i = 0; i < beans.getLength(); i++) {
            if (beans.item(i).getNodeName().equals("bean")) {
                try {
                    instanceOfClasses.put(classesInDir.getClass(beans.item(i).getAttributes().item(0).getTextContent()),getBeanInstance((Element) beans.item(i)));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getFieldNameToSet(Method method) {
        return method.getName().replace("set", "").toLowerCase();
    }


    public Map<Class, Object> getInstancesForLoader() {
        setInstanceOfClasses(xmlDocument.getDocumentElement());
        return instanceOfClasses;
    }
}
