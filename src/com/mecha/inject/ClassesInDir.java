package com.mecha.inject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassesInDir {

    private final String directory;
    private List<String> dirPathOfClasses = new ArrayList<>();
    private HashMap<String, Class> classes = new HashMap<>();

    public ClassesInDir(String directory) {
        this.directory = directory;
        getClasses();
    }

    public Class getClass(String className) throws ClassNotFoundException {
        if(classes.containsKey(className)) {
            return classes.get(className);
        } else {
            for (Map.Entry<String, Class> aClass: classes.entrySet()) {
                if (aClass.getKey().contains(className)) {
                    return classes.get(aClass.getKey());
                }
            }
        }
        throw new ClassNotFoundException(className+ "Not found.");
    }

//    private List<Class> getAnnotations() {
//        List<Class> annotations = new ArrayList<>();
//        for (Map.Entry<String, Class> classInPath: classes.entrySet()) {
//            if(classInPath.getValue().isAnnotation())
//                annotations.add(classInPath.getValue());
//        }
//        return annotations;
    }

    private List<Class> getClassesWithAnnotations(Class annotation) {
        List<Class> annotatedClass = new ArrayList<>();
        for (Map.Entry<String, Class> entry:getClasses().classes.entrySet()) {
            if (entry.getValue().isAnnotationPresent(annotation)) {
                annotatedClass.add(entry.getValue());
            }
        }
        return annotatedClass;
    }

    private ClassesInDir getClasses() {
        if (classes.size() <= 0) {
            HashMap<String, Class> classes = new HashMap<>();
            getClassesPathFromDir().classPathAsCanonicalNames().forEach(canonicalName -> {
                try {
                    Class aClass = Class.forName(canonicalName);
                    if (!aClass.isAnnotation())
                        classes.put(canonicalName, aClass);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            });
            this.classes = classes;
            return this;
        } else
            return this;
    }

    private ClassesInDir getClassesPathFromDir() {
        try (Stream<Path> walk = Files.walk(Paths.get(directory))) {
            dirPathOfClasses = walk.map(x -> x.toString())
                    .filter(f ->  f.endsWith(".java")).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    private List<String> classPathAsCanonicalNames() {
        String[] cannonicalNames = new String[this.dirPathOfClasses.size()];
        int index = 0;
        for (String path : dirPathOfClasses) {
            path = path.replaceAll(directory+"/"," ");
            path = path.replaceAll("/",".");
            path = path.replaceAll(".java","");
            path = path.trim();
            cannonicalNames[index] = path;
            index++;
        }
        return Arrays.asList(cannonicalNames);
    }
}
